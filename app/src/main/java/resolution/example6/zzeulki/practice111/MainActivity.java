package resolution.example6.zzeulki.practice111;

import android.opengl.GLSurfaceView;
import android.opengl.GLSurfaceView.Renderer;
import android.opengl.GLU;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.LinearLayout;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class MainActivity extends AppCompatActivity {

    private GLSurfaceView glSurface;
    private LinearLayout layout1;
    private LinearLayout layout2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        layout1.findViewById(R.id.layout_1);
 //       layout2.findViewById(R.id.layout_2);


        //glSurface = new GLSurfaceView(layout1.getContext());
        glSurface = new GLSurfaceView(this);
        glSurface.setRenderer(new MyRenderer());
        setContentView(glSurface);
    }

    @Override
    protected void onResume(){
        super.onResume();
        glSurface.onResume();
    }

    @Override
    protected void onPause(){
        super.onPause();
        glSurface.onPause();
    }

    class MyRenderer implements Renderer {
        private FloatBuffer vertexBuffer;
        private FloatBuffer vertexBuffer2;
        private float vertices[] = {
                0.0f, 2.0f, 0.0f,
                -1.0f, 0.0f, 0.0f,
                1.0f, 0.0f, 0.0f
        };

        private float vertices2[] = {
                -1.0f, -2.5f, 0.0f,
                1.0f, -2.5f, 0.0f,
                -1.0f, -0.5f, 0.0f,
                1.0f, -0.5f, 0.0f
        };

        public MyRenderer(){
            ByteBuffer byteBuf = ByteBuffer.allocateDirect(vertices.length*4);
            byteBuf.order(ByteOrder.nativeOrder());
            vertexBuffer = byteBuf.asFloatBuffer();
            vertexBuffer.put(vertices);
            vertexBuffer.position(0);

            ByteBuffer byteBuf2 = ByteBuffer.allocateDirect(vertices2.length*4);
            byteBuf2.order(ByteOrder.nativeOrder());
            vertexBuffer2 = byteBuf2.asFloatBuffer();
            vertexBuffer2.put(vertices2);
            vertexBuffer2.position(0);
        }

        @Override
        public void onSurfaceCreated(GL10 gl, EGLConfig config){
            gl.glShadeModel(GL10.GL_SMOOTH);
            gl.glClearColor(0.0f,0.0f,0.0f,0.5f);
            gl.glClearDepthf(1.0f);
            gl.glEnable(GL10.GL_DEPTH_TEST);
            gl.glDepthFunc(GL10.GL_LEQUAL);

            gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT,GL10.GL_NICEST);
        }

        @Override
        public void onSurfaceChanged(GL10 gl, int width,int height){
            if(height == 0){
                height = 1;
            }

            gl.glViewport(0,0,width,height);
            gl.glMatrixMode(GL10.GL_PROJECTION);
            gl.glLoadIdentity();

            GLU.gluPerspective(gl,45.0f,(float)width/(float)height,0.1f,100.0f);

            gl.glMatrixMode(GL10.GL_MODELVIEW);
            gl.glLoadIdentity();

        }

        @Override
        public void onDrawFrame(GL10 gl){
            gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);

            gl.glLoadIdentity();

            gl.glTranslatef(0.0f,0.0f,-6.0f);
            gl.glColor4f(1.0f,1.0f,0.0f,0.0f);
            gl.glFrontFace(GL10.GL_CW);
            gl.glVertexPointer(3,GL10.GL_FLOAT,0,vertexBuffer);
            gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
            gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP,0,vertices.length/3);
            gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);

            gl.glLoadIdentity();

            gl.glTranslatef(0.0f,0.0f,-6.0f);
            gl.glColor4f(0.0f,0.0f,1.0f,0.0f);
            gl.glFrontFace(GL10.GL_CW);
            gl.glVertexPointer(3,GL10.GL_FLOAT,0,vertexBuffer2);
            gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
            gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP,0,vertices2.length/3);
            gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
        }

    }
}
